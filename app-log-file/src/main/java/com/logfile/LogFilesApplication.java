package com.logfile;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableScheduling
@SpringBootApplication(scanBasePackageClasses = { LogFilesApplication.class })
public class LogFilesApplication extends SpringBootServletInitializer {
	private static final Logger logger = LoggerFactory.getLogger(LogFilesApplication.class);
	public static final String INPUT_FILE_PATH_NAME_PROPERTY = "INPUT_FILE_PATH";

	public static void main(String[] args) {
		logger.info("starting main");
		SpringApplicationBuilder builder = new SpringApplicationBuilder();
		builder.properties(createConfigLocationProperties());
		builder.sources(LogFilesApplication.class).run(args);
		// SpringApplication.run(BfgGuiApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		logger.info("starting configure");
		return builder.sources(LogFilesApplication.class).properties(createConfigLocationProperties());
	}

	private static Map<String, Object> createConfigLocationProperties() {
		logger.info("starting createConfigLocationProperties");
		String inputFilePath = System.getProperty(INPUT_FILE_PATH_NAME_PROPERTY);
		final Map<String, Object> properties = new HashMap<>();
		logger.debug("inputFilePath from application argument = " + inputFilePath);
		properties.put(INPUT_FILE_PATH_NAME_PROPERTY, inputFilePath);
		return properties;
	}
}
