package com.logfile.bd;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.logfile.dto.JsonFileLineWrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "EVENT")
public class Event {
	private final static int TYPE_AND_HOST_LENGTH = 4000;
	@Id
	@Column(name = "id", length = 255)
	private String id;

	@Column(name = "duration", nullable = false)
	private long duration;

	@Column(name = "alert", nullable = false)
	private boolean alert;

	@Column(name = "type", length = TYPE_AND_HOST_LENGTH)
	private String type;
	@Column(name = "host", length = TYPE_AND_HOST_LENGTH)
	private String host;
	
	@Version
	private long version;

	public Event(JsonFileLineWrapper earlierEvent, JsonFileLineWrapper event) {
		this.id = earlierEvent.getId();
		long earlierTimestamp = earlierEvent.getTimestamp();
		long timestamp = event.getTimestamp();
		this.duration = timestamp - earlierTimestamp;
		this.alert = this.duration > 4;
		this.type = earlierEvent.getType();
		this.host = earlierEvent.getHost();
	}
}
