package com.logfile.services;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logfile.bd.Event;
import com.logfile.dao.EventDAO;

@Service
public class EventService { 
    private static final Logger logger = LoggerFactory.getLogger(EventService.class);
    @Autowired
    private EventDAO eventDAO;
    @Autowired  EntityManager em;
    
    // dont need transactional because dao is transactional
    public void save(Event e) {
    	logger.debug("start store entity");
    	eventDAO.save(e);
    	logger.debug("entity is stored successfully");
    }
    @PostConstruct
    public void init() {
    	String jakisSyf = "333";
    	jakisSyf.charAt(1);
    }
    
}
