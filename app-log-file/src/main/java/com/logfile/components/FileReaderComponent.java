package com.logfile.components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logfile.LogFilesApplication;
import com.logfile.bd.Event;
import com.logfile.dto.JsonFileLineWrapper;
import com.logfile.services.EventService;

@Component
public class FileReaderComponent {
	private static final Logger logger = LoggerFactory.getLogger(FileReaderComponent.class);

	@Value("${" + LogFilesApplication.INPUT_FILE_PATH_NAME_PROPERTY + "}")
	private String inputFilePath;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;
	@Autowired
	private EventService eventService;

	private Map<String, JsonFileLineWrapper> linesEventsMap = new HashMap<>();

	@PostConstruct
	public void init() {
		AtomicInteger launchedTasks = new AtomicInteger(0);
		try (InputStream inputStream = Files.newInputStream(Paths.get(inputFilePath));
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			logger.info("start reading input file");

			String line = null;
			while ((line = reader.readLine()) != null) {
				JsonFileLineWrapper jsonFileLineWrapper = getJSon(line);
				JsonFileLineWrapper jsonWrapperFromMap = linesEventsMap.get(jsonFileLineWrapper.getId());
				if (jsonWrapperFromMap != null) {
					logger.debug("executor launching");
					launchedTasks.incrementAndGet();
					taskExecutor.execute(() -> {
						logger.info("executor started");
						try {
							Event e = jsonWrapperFromMap.create(jsonFileLineWrapper);
							eventService.save(e);
						} catch (Exception e) {
							logger.error(e.getMessage());
						}
						launchedTasks.decrementAndGet();
					});
				} else {
					logger.debug("put on map");
					linesEventsMap.put(jsonFileLineWrapper.getId(), jsonFileLineWrapper);
				}
			}
			logger.info("input file readed and preprocessed successfully");
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.debug("waiting for tasks finish");
		new Thread() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(800);
					} catch (InterruptedException e) {
						logger.warn(e.getMessage());
					}
					new Thread() {
						public void run() {
							if (launchedTasks.intValue() == 0) {
								logger.debug("exiting");
								// System.exit(0);
							}
						}
					}.start();
				}
			}
		}.start();
	}

	private JsonFileLineWrapper getJSon(String lineJson) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = mapper.readValue(lineJson, Map.class);
		return new JsonFileLineWrapper(map);
	}
}
