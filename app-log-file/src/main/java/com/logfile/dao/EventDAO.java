package com.logfile.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logfile.bd.Event;

@Repository
public interface EventDAO extends JpaRepository<Event, String> {

}
