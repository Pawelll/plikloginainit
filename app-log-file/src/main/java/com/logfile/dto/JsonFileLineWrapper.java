package com.logfile.dto;

import java.util.Map;

import com.logfile.bd.Event;

public class JsonFileLineWrapper {
	private static final String KEY_ID = "id";
	private static final String KEY_STATE = "state";
	private static final String KEY_TIMESTAMP = "timestamp";
	private static final String KEY_TYPE = "type";
	private static final String KEY_HOST = "host";

	private static final String STATE_STARTED = "STARTED";

	private final Map<String, String> jsonMap;

	public JsonFileLineWrapper(Map<String, String> jsonMap) {
		this.jsonMap = jsonMap;
	}

	public String getId() {
		return jsonMap.get(KEY_ID);
	}

	public String getState() {
		return jsonMap.get(KEY_STATE);
	}

	public long getTimestamp() {
		Object ts = jsonMap.get(KEY_TIMESTAMP);
		long timestamp = (long) ts;
		return timestamp;
	}

	public String getType() {
		return jsonMap.get(KEY_TYPE);
	}

	public String getHost() {
		return jsonMap.get(KEY_HOST);
	}

	public Event create(JsonFileLineWrapper coupledLine) {
		return STATE_STARTED.equals(getState()) ? new Event(this, coupledLine) : new Event(coupledLine, this);
	}
}
