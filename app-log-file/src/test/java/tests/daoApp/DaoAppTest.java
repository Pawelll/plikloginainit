package tests.daoApp;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.logfile.dao.EventDAO;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { DaoApp.class })
public class DaoAppTest {

	@Autowired
	private EventDAO eventDAO;

	@Value("${logging.path}")
	private String loggingPathValue;

	@Test
	void testNobody() {
		System.out.println("!!! end, loggingPathValue: '" + loggingPathValue + "'");
	}

	@Test
	void testAssertTrue() {
		assertTrue(true, "assert tue sie skleszczyl");
	}

}
