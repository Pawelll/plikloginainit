package tests.daoApp;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.logfile.bd.Event;
import com.logfile.dao.EventDAO;

@SpringBootApplication(scanBasePackageClasses = { DaoApp.class })
@EntityScan(basePackageClasses = Event.class)
@EnableJpaRepositories(basePackageClasses = EventDAO.class)
//@ComponentScan(basePackageClasses = FileReaderComponent.class)
public class DaoApp {
}
