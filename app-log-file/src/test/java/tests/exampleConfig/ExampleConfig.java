package tests.exampleConfig;

import org.assertj.core.util.Arrays;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class ExampleConfig implements ApplicationContextAware {
	private ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.context = context;
		printBeansList();
	}

	private void printBeansList() {
		System.out.println("__________ lista beanow: ____");
		Arrays.asList(context.getBeanDefinitionNames()).forEach(n -> System.out.println((String) n));
		System.out.println("__________ koniec listy beanow ____");
	}

	@Bean(name = "kontekscik")
	public ApplicationContext[] cgetApplicationContext() {
		ApplicationContext[] c = { context };
		return c;
	}
}
