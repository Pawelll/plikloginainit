package tests.exampleConfig;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { ExampleConfig.class })
//@SpringBootTest(classes = { ConfigDAO.class })
public class ExampleConfigTest {

	@Value("${logging.path}")
	private String loggingPath;

	@BeforeAll
	public static void setUp() {
	}

	@AfterAll
	public static void end() {
		System.out.println("Runtime.getRuntime().maxMemory()=" + Runtime.getRuntime().maxMemory());
	}

	@Test
	void testPrintAppName() {
		// fail("Not yet implemented");
		System.out.println("loggingPath = '" + loggingPath + "'");
		assertTrue(true, "testPrintAppName message on error");
	}
}
